@include('user.layout.header')

    <!-- ##### Breadcrumb Area Start ##### -->
    <div class="breadcrumb-area bg-img bg-overlay" style="background-image: url({{asset('egames/img/background/aldous.png')}});">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <!-- Breadcrumb Text -->
                <div class="col-12">
                    <div class="breadcrumb-text">
                        <h2>User Guide</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Breadcrumb Area End ##### -->

<!-- ##### Monthly Picks Area Start ##### -->
<section class="monthly-picks-area section-padding-100 bg-pattern">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="left-right-pattern"></div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <!-- Title -->
                <h2 class="section-title mb-70 wow fadeInUp" data-wow-delay="100ms">Hero Guide.</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <ul class="nav nav-tabs wow fadeInUp" data-wow-delay="300ms" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="popular-tab" data-toggle="tab" href="#popular" role="tab"
                            aria-controls="popular" aria-selected="true">All</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="latest-tab" data-toggle="tab" href="#latest" role="tab"
                            aria-controls="latest" aria-selected="false">Mage</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="editor-tab" data-toggle="tab" href="#editor" role="tab"
                            aria-controls="editor" aria-selected="false">Assassin</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="tab-content wow fadeInUp" data-wow-delay="500ms" id="myTabContent">
        <div class="tab-pane fade show active" id="popular" role="tabpanel" aria-labelledby="popular-tab">
            <!-- Popular Games Slideshow -->
            <div class="popular-games-slideshow owl-carousel">

                <!-- Single Games -->
                <div class="single-games-slide">
                    <img src="{{asset('egames/img/bg-img/50.jpg')}}" alt="">
                    <div class="slide-text">
                        <a href="#" class="game-title">Grand Theft Auto V</a>
                        <div class="meta-data">
                            <a href="#">User: 9.1/10</a>
                            <a href="#">Action</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="tab-pane fade" id="latest" role="tabpanel" aria-labelledby="latest-tab">
            <!-- Latest Games Slideshow -->
            <div class="latest-games-slideshow owl-carousel">

                <!-- Single Games -->
                <div class="single-games-slide">
                    <img src="{{asset('egames/img/bg-img/50.jpg')}}" alt="">
                    <div class="slide-text">
                        <a href="#" class="game-title">Grand Theft Auto V</a>
                        <div class="meta-data">
                            <a href="#">User: 9.1/10</a>
                            <a href="#">Action</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="tab-pane fade" id="editor" role="tabpanel" aria-labelledby="editor-tab">
            <!-- Editor Games Slideshow -->
            <div class="editor-games-slideshow owl-carousel">

                <!-- Single Games -->
                <div class="single-games-slide">
                    <img src="{{asset('egames/img/bg-img/50.jpg')}}" alt="">
                    <div class="slide-text">
                        <a href="#" class="game-title">Grand Theft Auto V</a>
                        <div class="meta-data">
                            <a href="#">User: 9.1/10</a>
                            <a href="#">Action</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
@include('user.layout.footer')