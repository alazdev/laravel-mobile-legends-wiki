<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>View - Hero</title>

    <!-- Stylesheet -->
    <link rel="stylesheet" href="{{asset('egames/style.css')}}">

</head>
    <section class="single-game-review-area section-padding-100">
        <div class="container">
            <a href="{{route('admin-hero-index')}}" class="btn btn-dark">Back</a>
            <div class="row align-items-center">
                <!-- *** Review Area *** -->
                <div class="col-12 col-md-6">
                    <div class="single-game-review-area style-2 mt-70">
                        <div class="game-content">
                            <a href="#" class="game-title" style="text-transform: capitalize;">{{$cek->hero_name}}</a>
                            <div class="game-meta">
                                <a href="#" class="game-comments">( {{$cek->role1}} {{$cek->role2 != null ? '/ '.$cek->role2 : ''}} )</a>
                            </div>
                            <p>{{$cek->bio}}.</p>
                            <!-- Download & Rating Area -->
                            <div class="download-rating-area">
                                <div class="download-area">
                                    <a href="http://mlbb.app.link/Pj8LAOHy"><img src="{{asset('egames/img/core-img/app-store.png')}}" alt=""></a>
                                    <a href="http://mlbb.app.link/Pj8LAOHy"><img src="{{asset('egames/img/core-img/google-play.png')}}" alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- *** Barfiller Area *** -->
                <div class="col-12 col-md-6">
                    <div class="egames-barfiller">
                        <!-- Single Barfiller -->
                        <div class="single-barfiller-area">
                            <div id="bar1" class="barfiller">
                                <span class="tip"></span>
                                <span class="fill" data-percentage="{{$cek->durability}}"></span>
                                <p>Durability</p>
                            </div>
                        </div>
                        <!-- Single Barfiller -->
                        <div class="single-barfiller-area">
                            <div id="bar2" class="barfiller">
                                <span class="tip"></span>
                                <span class="fill" data-percentage="{{$cek->offense}}"></span>
                                <p>Offense</p>
                            </div>
                        </div>
                        <!-- Single Barfiller -->
                        <div class="single-barfiller-area">
                            <div id="bar3" class="barfiller">
                                <span class="tip"></span>
                                <span class="fill" data-percentage="{{$cek->ability_effects}}"></span>
                                <p>Ability Effects</p>
                            </div>
                        </div>
                        <!-- Single Barfiller -->
                        <div class="single-barfiller-area">
                            <div id="bar4" class="barfiller">
                                <span class="tip"></span>
                                <span class="fill" data-percentage="{{$cek->difficulty}}"></span>
                                <p>Difficulty</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
    <script src="{{asset('egames/js/jquery/jquery-2.2.4.min.js')}}"></script>
    <!-- Popper js -->
    <script src="{{asset('egames/js/bootstrap/popper.min.js')}}"></script>
    <!-- Bootstrap js -->
    <script src="{{asset('egames/js/bootstrap/bootstrap.min.js')}}"></script>
    <!-- All Plugins js -->
    <script src="{{asset('egames/js/plugins/plugins.js')}}"></script>
    <!-- Active js -->
    <script src="{{asset('egames/js/active.js')}}"></script>
</body>

</html>