<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HeroesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('heroes')->insert([
            'hero_name' => Str::random(10),
            'logo' => Str::random(10),
            'durability' => 100,
            'offense' => 100,
            'ability_effects' => 100,
            'difficulty' => 100,
            'role1' => 1,
            'role2' => 2,
            'bio' => Str::random(10),
        ]);
    }
}
