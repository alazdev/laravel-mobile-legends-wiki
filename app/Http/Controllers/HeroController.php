<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\PayUService\Exception;
use App\Hero;
use App\HeroRole;
use DataTables;

class HeroController extends Controller
{
    public function index()
    {
        return view('user.hero.index');
    }
    public function admin_hero_index()
    {
        $role   = HeroRole::get()->count();
        return view('admin.hero.index', compact('role'));
    }
    public function admin_hero_data()
    {
        $heroes = Hero::get();
        return DataTables::of($heroes)
            ->addColumn('new_bio', function($heroes){
                return str_limit($heroes->bio, 25, '...');
            })
            ->addColumn('action', function($heroes){
                return '<a href="hero/view/'.$heroes->id.'/'.$heroes->hero_name.'" class="btn btn-success"><i class="fa fa-eye"></i></a> <a href="hero/edit/'.$heroes->id.'/'.$heroes->hero_name.'" class="btn btn-primary"><i class="fa fa-pencil-square-o"></i></a> <a href="hero/delete/'.$heroes->id.'/'.$heroes->hero_name.'" class="btn btn-danger"><i class="fa fa-trash"></i></a><input type="checkbox" name="user_checkbox[]" class="user_checkbox pull-right" value="'.$heroes->id.'" />';
            })
            ->make(true);
    }
    public function admin_hero_create(Request $request){
        try{
            $hero   = new Hero();
            $hero->hero_name    = $request->input('hero_name');
            $hero->logo         = $request->input('logo');
            $hero->durability   = $request->input('durability');
            $hero->offense      = $request->input('offense');
            $hero->ability_effects  = $request->input('ability_effects');
            $hero->difficulty   = $request->input('difficulty');
            $hero->role1        = $request->input('role1');
            $hero->role2        = $request->input('role2');
            $hero->bio          = $request->input('bio');
            $hero->save();
            return(back());
        } catch (\Exception $e){
            return $e->getMessage();
        }
    }
    public function admin_hero_edit($id, $heroName){
        try{
            $cek    = Hero::find($id);
            if($cek->hero_name == $heroName){
                return view('admin.hero.edit', compact('cek'));
            } else {
                echo 'ini gak cocok bro';
            }
        } catch (\Exception $e){
            return $e->getMessage();
        }
    }
    public function admin_hero_update(Request $request, $id, $heroName){
        try {
            $hero    = Hero::find($id);
            if($hero->hero_name == $heroName){
                $hero->hero_name    = $request->input('hero_name');
                if($request->input('logo')!= null){
                    $hero->logo         = $request->input('logo');
                }
                $hero->durability   = $request->input('durability');
                $hero->offense      = $request->input('offense');
                $hero->ability_effects  = $request->input('ability_effects');
                $hero->difficulty   = $request->input('difficulty');
                $hero->role1        = $request->input('role1');
                $hero->role2        = $request->input('role2');
                $hero->bio          = $request->input('bio');
                $hero->save();
                return redirect()->route('admin-hero-index');
            } else {
                echo 'ini gak cocok bro';
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
    public function admin_hero_view($id, $heroName){
        try{
            $cek    = Hero::find($id);
            if($cek->hero_name == $heroName){
                return view('admin.hero.view', compact('cek'));
            } else {
                echo 'ini gak cocok bro';
            }
        } catch (\Exception $e){
            return $e->getMessage();
        }
    }
    public function admin_hero_destroy($id, $heroName){
        try{
            $cek    = Hero::find($id);
            if($cek->hero_name == $heroName){
                $cek->delete($id);
                return(back());
            } else {
                echo 'ini gak cocok bro';
            }
        } catch (\Exception $e){
            return $e->getMessage();
        }
    }
    public function admin_hero_massdelete(Request $req)
    {
        $hero_id_array = $req->input('id');
        $hero = Hero::whereIn('id', $hero_id_array);
        $hero->delete();
    }
}
